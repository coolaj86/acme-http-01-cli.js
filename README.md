# [acme-http-01-cli](https://git.rootprojects.org/root/acme-http-01-cli.js) | a [Root](https://rootprojects.org) project

An extremely simple reference implementation
of an ACME (Let's Encrypt) http-01 challenge strategy.

This generic implementation can be adapted to work with any node.js ACME client,
although it was built for [Greenlock](https://git.rootprojects.org/root/greenlock-express.js)
and [ACME.js](https://git.rootprojects.org/root/acme-v2.js).

```
GET http://example.com/.well-known/acme-challenge/xxxxxxxxxxxxxxxx
```

* Prints the ACME challenge URL and Key Authorization to the terminal
  * (waits for you to hit enter before continuing)
* Asks you to enter (or upload) the Key Authorization challenge response.
* Let's you know when the challenge as succeeded or failed, and is safe to remove.

Other ACME Challenge Reference Implementations:

* [acme-http-01-fs](https://git.rootprojects.org/root/acme-http-01-webroot.js.git)
* [**acme-http-01-cli**](https://git.rootprojects.org/root/acme-http-01-cli.js.git)
* [acme-dns-01-cli](https://git.rootprojects.org/root/acme-dns-01-cli.js.git)

Install
-------

```bash
npm install --save acme-http-01-cli@3.x
```

Usage
-----

```bash
var Greenlock = require('greenlock');

Greenlock.create({
  ...
, challenges: { 'http-01': require('acme-http-01-cli').create({ debug: true })
              , 'dns-01': require('acme-dns-01-cli')
              , 'tls-alpn-01': require('acme-tls-alpn-01-cli')
              }
  ...
});
```

Note: If you request a certificate with 6 domains listed,
it will require 6 individual challenges.

Exposed (Promise) Methods
---------------

For ACME Challenge:

* `set(opts)`
* `remove(opts)`

The `http-01` strategy does not support wildcard domains (whereas `dns-01` does).
The options will look like this (which you can see when `debug: true` is set):

```js
{ challenge: {
    type: 'http-01'
  , identifier: { type: 'dns', value: 'example.com' }
  , wildcard: false
  , expires: '2012-01-01T12:00:00.000Z'
  , token: 'abc123'
  , thumbprint: '<<account key thumbprint>>'
  , keyAuthorization: 'abc123.xxxx'
  , dnsHost: '_acme-challenge.example.com'
  , dnsAuthorization: 'yyyy'
  , altname: 'example.com'
  }
}
```

Optional:

* `get(limitedOpts)`

Because the get method is apart from the main flow (such as a DNS query),
it's not always implemented and the options are much more limited in scope:

```js
{ challenge: {
    type: 'http-01'
  , identifier: { type: 'dns', value: 'example.com' }
  , wildcard: false
  , token: 'abc123'
  , altname: 'example.com'
  }
}
```

# Legal &amp; Rules of the Road

Greenlock&trade; and Bluecrypt&trade; are [trademarks](https://rootprojects.org/legal/#trademark) of AJ ONeal

The rule of thumb is "attribute, but don't confuse". For example:

> Built with [Greenlock](https://git.rootprojects.org/root/greenlock.js) (a [Root](https://rootprojects.org) project).

Please [contact us](mailto:aj@therootcompany.com) if you have any questions in regards to our trademark,
attribution, and/or visible source policies. We want to build great software and a great community.

[Greenlock&trade;](https://git.rootprojects.org/root/greenlock.js) |
MPL-2.0 |
[Terms of Use](https://therootcompany.com/legal/#terms) |
[Privacy Policy](https://therootcompany.com/legal/#privacy)
