'use strict';

var tester = require('greenlock-challenge-test');

var type = 'http-01';
//var challenger = require('greenlock-challenge-http').create({});
var challenger = require('./').create({});

// The dry-run tests can pass on, literally, 'example.com'
// but the integration tests require that you have control over the domain
var domain = 'example.com';

tester.test(type, domain, challenger).then(function () {
  console.info("PASS");
}).catch(function (err) {
  console.error("FAIL");
  console.error(err);
  process.exit(19);
});
